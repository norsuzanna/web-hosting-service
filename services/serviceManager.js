const { exec } = require('child_process');
const db = require('../database');
const axios = require('axios');

const portRange = { start: 10000, end: 11000 };
let currentPort = portRange.start;

const getNextAvailablePort = () => {
    if (currentPort > portRange.end) throw new Error('No available ports');
    return currentPort++;
};

const createService = (repoUrl) => {
    return new Promise((resolve, reject) => {
        const port = getNextAvailablePort();
        const containerName = `service_${port}`;

        const command = `docker run -d -e REPO_URL=${repoUrl} -p ${port}:3000 --name ${containerName} node:14 sh -c "git clone ${repoUrl} app && cd app && npm install && node app.js"`;

        exec(command, (error, stdout, stderr) => {
            if (error) return reject(error);
            const containerId = stdout.trim();

            db.run(
                'INSERT INTO services (repo_url, container_id, port) VALUES (?, ?, ?)',
                [repoUrl, containerId, port],
                function (err) {
                    if (err) return reject(err);
                    resolve({ id: this.lastID, repoUrl, containerId, port });
                }
            );
        });
    });
};

const getServiceById = (id) => {
    return new Promise((resolve, reject) => {
        db.get('SELECT * FROM services WHERE id = ?', [id], (err, row) => {
            if (err) return reject(err);
            resolve(row);
        });
    });
};

module.exports = { createService, getServiceById };
