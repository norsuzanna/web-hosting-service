const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('./data/services.db');

db.serialize(() => {
    db.run(`CREATE TABLE IF NOT EXISTS services (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        repo_url TEXT NOT NULL,
        container_id TEXT NOT NULL,
        port INTEGER NOT NULL
    )`);
});

module.exports = db;
