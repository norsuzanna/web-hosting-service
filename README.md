# web-hosting-service



## Start the Express.js server:

```
node app.js
```

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Test the API
### Register a new service:
```
curl -X POST http://localhost:3000/api/services/register -H "Content-Type: application/json" -d '{"repoUrl":"https://github.com/user/repo"}'
```

### Register a new service:
```
curl http://localhost:3000/api/services/1
```