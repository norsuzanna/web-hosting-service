const express = require('express');
const bodyParser = require('body-parser');
const serviceRoutes = require('./routes/services');

const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use('/api/services', serviceRoutes);

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
