FROM node:14

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./

RUN npm install

# Bundle app source
COPY . .

# Run the service
CMD ["sh", "-c", "git clone $REPO_URL app && cd app && npm install && node app.js"]
