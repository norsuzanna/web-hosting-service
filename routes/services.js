const express = require('express');
const router = express.Router();
const serviceManager = require('../services/serviceManager');

router.post('/register', async (req, res) => {
    const { repoUrl } = req.body;

    try {
        const service = await serviceManager.createService(repoUrl);
        res.status(201).json({ message: 'Service created', service });
    } catch (error) {
        res.status(500).json({ message: 'Error creating service', error: error.message });
    }
});

router.get('/:id', async (req, res) => {
    const { id } = req.params;

    try {
        const service = await serviceManager.getServiceById(id);
        if (service) {
            res.status(200).json(service);
        } else {
            res.status(404).json({ message: 'Service not found' });
        }
    } catch (error) {
        res.status(500).json({ message: 'Error retrieving service', error: error.message });
    }
});

module.exports = router;
